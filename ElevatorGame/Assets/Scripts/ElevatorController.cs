﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;


public enum Door
{
    ForwardTop,
    ForwardBottom,
    BackwardsTop,
    BackwardsBottom,
    RightTop,
    RightBottom,
    LeftTop,
    LeftBottom
}
public class ElevatorController : MonoBehaviour
{

    public bool CloseDoors;
    public float DoorSpeed;

    private Vector3 _moveDoors;
    private IDictionary<Door, GameObject> _doors;
    private IDictionary<Door, Vector3> _doorsDefaultPosition;
    private IDictionary<Door, Vector3> _doorsTargetPosition;

    // Use this for initialization
    void Start ()
    {
        _moveDoors = new Vector3(0f, 1.25f, 0f);
        MapDoors();
        MapDoorsDefaultPosition();
        _doorsTargetPosition = new Dictionary<Door, Vector3>();
        //Doors open!
        _moveDoors = new Vector3(0f, 1.25f, 0f);
        MapDoorsTargetPosition();
        CloseDoors = false;
    }
	
	// Update is called once per frame
	void Update () {
        OpenDoors();
	    if (CloseDoors)
	    {
            End();
	        CloseDoors = false;
	    }
	}
    public void OpenDoors()
    {
        foreach (var i in Enum.GetValues(typeof(Door)).Cast<Door>().ToList())
        {
            _doors[i].transform.position = Vector3.MoveTowards(_doors[i].transform.position, _doorsTargetPosition[i],
                DoorSpeed);
        }
    }

    private void MapDoors()
    {
        _doors = new Dictionary<Door, GameObject>();
        foreach (var i in GameObject.FindGameObjectsWithTag("ElevatorDoor"))
        {
            Door door = Door.RightBottom;
            switch (i.name)
            {
                case ("ForwardTop"):
                    door = Door.ForwardTop;
                    break;
                case ("ForwardBottom"):
                    door = Door.ForwardBottom;
                    break;
                case ("BackwardsTop"):
                    door = Door.BackwardsTop;
                    break;
                case ("BackwardsBottom"):
                    door = Door.BackwardsBottom;
                    break;
                case ("RightTop"):
                    door = Door.RightTop;
                    break;
                case ("RightBottom"):
                    door = Door.RightBottom;
                    break;
                case ("LeftTop"):
                    door = Door.LeftTop;
                    break;
                case ("LeftBottom"):
                    door = Door.LeftBottom;
                    break;
                default:
                    break;
            }
            _doors.Add(door, i);
        }
    }

    private void MapDoorsDefaultPosition()
    {
        _doorsDefaultPosition = new Dictionary<Door, Vector3>
        {
            {Door.ForwardTop, _doors[Door.ForwardTop].transform.position},
            {Door.ForwardBottom, _doors[Door.ForwardBottom].transform.position},
            {Door.BackwardsTop, _doors[Door.BackwardsTop].transform.position},
            {Door.BackwardsBottom, _doors[Door.BackwardsBottom].transform.position},
            {Door.RightTop, _doors[Door.RightTop].transform.position},
            {Door.RightBottom, _doors[Door.RightBottom].transform.position},
            {Door.LeftTop, _doors[Door.LeftTop].transform.position},
            {Door.LeftBottom, _doors[Door.LeftBottom].transform.position}
        };
    }

    private void MapDoorsTargetPosition()
    {
        _doorsTargetPosition.Add(Door.ForwardTop, _doorsDefaultPosition[Door.ForwardTop] + _moveDoors);
        _doorsTargetPosition.Add(Door.ForwardBottom, _doorsDefaultPosition[Door.ForwardBottom] - _moveDoors);
        _doorsTargetPosition.Add(Door.BackwardsTop, _doorsDefaultPosition[Door.BackwardsTop] + _moveDoors);
        _doorsTargetPosition.Add(Door.BackwardsBottom, _doorsDefaultPosition[Door.BackwardsBottom] - _moveDoors);
        _doorsTargetPosition.Add(Door.RightTop, _doorsDefaultPosition[Door.RightTop] + _moveDoors);
        _doorsTargetPosition.Add(Door.RightBottom, _doorsDefaultPosition[Door.RightBottom] - _moveDoors);
        _doorsTargetPosition.Add(Door.LeftTop, _doorsDefaultPosition[Door.LeftTop] + _moveDoors);
        _doorsTargetPosition.Add(Door.LeftBottom, _doorsDefaultPosition[Door.LeftBottom] - _moveDoors);
    }

    private void End()
    {
        _doorsTargetPosition[Door.ForwardTop] = _doorsDefaultPosition[Door.ForwardTop];
        _doorsTargetPosition[Door.ForwardBottom] = _doorsDefaultPosition[Door.ForwardBottom];
        _doorsTargetPosition[Door.BackwardsTop] = _doorsDefaultPosition[Door.BackwardsTop];
        _doorsTargetPosition[Door.BackwardsBottom] = _doorsDefaultPosition[Door.BackwardsBottom];
        _doorsTargetPosition[Door.RightTop] = _doorsDefaultPosition[Door.RightTop];
        _doorsTargetPosition[Door.RightBottom] = _doorsDefaultPosition[Door.RightBottom];
        _doorsTargetPosition[Door.LeftTop] = _doorsDefaultPosition[Door.LeftTop];
        _doorsTargetPosition[Door.LeftBottom] = _doorsDefaultPosition[Door.LeftBottom];
    }
}
