﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;
using Newtonsoft.Json.Serialization;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

namespace Assets.Scripts
{
    public class ConfigController
    {
        private static Config _config;
        private static string _path = @"config.json";

        public ConfigController()
        {
            LoadConfig();
        }

        /// <summary>
        /// Generates the default config setup.
        /// </summary>
        /// <returns></returns>
        private static Config GenerateDefaultConfig()
        {
            Config config = new Config()
            {
                Monitors = new List<Monitor>()
                {
                    new Monitor() {GameDisplay = Display.Main, SystemDisplayIndex = 0},
                    new Monitor() {GameDisplay = Display.Forward, SystemDisplayIndex = 1},
                    new Monitor() {GameDisplay = Display.Right, SystemDisplayIndex = 2},
                    new Monitor() {GameDisplay = Display.Backwards, SystemDisplayIndex = 3},
                    new Monitor() {GameDisplay = Display.Left, SystemDisplayIndex = 4}
                }
            };
            return config;
        }

        public void Go()
        {

        }

        public void GetDisplayIndex(Display display)
        {

        }

        public void LoadConfig()
        {
            if (IsConfigOnDisk(_path))
            {
                Config config = GetConfigFromDisk(_path);
                //If config is NOT NULL, set _config to config. Else set it to output from GenerateDefaultConfig. ?? is the "null-coalescing operator".
                _config = config ?? GenerateDefaultConfig();
            }
            else
            {
                _config = GenerateDefaultConfig();
                WriteConfigToDisk(_config, _path);
            }
        }

        /// <summary>
        /// Checks if the config file already exists on the disk. Doesn't check if it's valid.
        /// </summary>
        /// <returns>true if the file exists, false if not.</returns>
        private bool IsConfigOnDisk(string path)
        {
            return File.Exists(path);
        }

        private bool IsJsonValid(string json, Config config)
        {
            //TODO Validate config.
            //JSchema schema = new JSchemaGenerator().Generate(typeof(Config));
            //JObject jobject = JObject.Parse(json);
            //bool value = jobject.IsValid(schema);

            return true;
        }

        /// <summary>
        /// Gets the config file from disk
        /// </summary>
        /// <param name="path">The Path to load the config from.</param>
        /// <returns>The Config instance or null if it went wrong.</returns>
        private Config GetConfigFromDisk(string path)
        {
            //TODO make it load the config file from disk.
            string json = File.ReadAllText(path);
            JsonConvert.DeserializeObject<Config>(json);

            return null;
        }

        /// <summary>
        /// extension that validates if Json string is copmplient to TSchema.
        /// </summary>
        /// <typeparam name="TSchema">schema</typeparam>
        /// <param name="value">json string</param>
        /// <returns>is valid?</returns>
        //public static bool IsJsonValid<TSchema>(this string value)
        //    where TSchema : new()
        //{
        //    bool res = true;
        //    //this is a .net object look for it in msdn
        //    var ser = new JsonConvert.SerializeObject();
        //    //first serialize the string to object.
        //    var obj = ser.Deserialize<TSchema>(value);

        //    //get all properties of schema object
        //    var properties = typeof(TSchema).GetProperties();
        //    //iterate on all properties and test.
        //    foreach (PropertyInfo info in properties)
        //    {
        //        // i went on if null value then json string isnt schema complient but you can do what ever test you like her.
        //        var valueOfProp = obj.GetType().GetProperty(info.Name).GetValue(obj, null);
        //        if (valueOfProp == null)
        //            res = false;
        //    }

        //    return res;
        //}

        /// <summary>
        /// Writes a config file to the disk. Will overwrite and existing file. 
        /// Doesn't handle errors, but writes error information to Unity debug log.
        /// </summary>
        /// <param name="config">The Object of type Config to write to the disk</param>
        /// <param name="path">The realtive path and file it shall write to.</param>
        private void WriteConfigToDisk(Config config, string path)
        {
            bool error = false;
            string errorMessage = "Undefined error";
            try
            {
                string json = JsonConvert.SerializeObject(config, Formatting.Indented);
                File.WriteAllText(path, json);
                if (!IsConfigOnDisk(path))
                {
                    error = true;
                }
            }
            catch (Exception exception)
            {
                error = true;
                errorMessage = "Error message: " + exception.Message + "\n StackTrace: " + exception.StackTrace;
            }
            if (error) Debug.LogError("Error writing config to disk. Details: \n" + errorMessage);
        }
    }
}
