﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class Utility
    {
        public static float PercentageToValue(float percentage, float minValue, float maxValue)
        {
            var val = (percentage * (maxValue - minValue) / 100) + minValue;
            return val;
        }

        public static float ValuesToPercentage(float minValue, float maxValue, float currentValue)
        {
            var percentage = (currentValue - minValue) / (maxValue - minValue) * 100;
            percentage = Mathf.Abs(percentage);
            //print("Percentage: " + percentage);
            return percentage;
        }
    }
}
