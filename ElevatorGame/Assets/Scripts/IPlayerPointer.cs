﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public interface IPlayerPointer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerXAxis"></param>
        /// <param name="controllerYAxis"></param>
        void SetControlls(string controllerXAxis, string controllerYAxis);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool IsInteractiveObjectInFront();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IInteractive GetInteractiveObjectInFront();
    }
}
