﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace Assets.Scripts
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum JoyStickControl
    {
        LeftStick_X,
        LeftStick_Y,
        RightStick_X,
        RightStick_Y,
        DPad_X,
        DPad_Y,
        Triggers,
        LeftTrigger,
        RightTrigger,
        Button_A,
        Button_B,
        Button_X,
        Button_Y
    }

    public enum JoyStick
    {
        JoyStick1,
        JoyStick2,
        JoyStick3,
        JoyStick4
    }

    public class ControlsMapping
    {
        private IDictionary<JoyStick, IDictionary<JoyStickControl, string>> _joySticks; //TODO: Change the rest of the class to use this field instead.
        private IDictionary<JoyStick, bool> _isJoyStickTaken; //TODO: make use of this field instead.
        public IDictionary<JoyStickControl, string> Keyboard { get; private set; }
        private static IDictionary<JoyStickControl, string> _joystick1;
        private static IDictionary<JoyStickControl, string> _joystick2;
        private static IDictionary<JoyStickControl, string> _joystick3;
        private static IDictionary<JoyStickControl, string> _joystick4;

        private bool _joyStick1IsTaken;
        private bool _joyStick2IsTaken;
        private bool _joyStick3IsTaken;
        private bool _joyStick4IsTaken;
        private static ControlsMapping _instance;
        private static readonly object Threadlock = new object();

        private ControlsMapping()
        {
            MapControlls();
        }

        /// <summary>
        /// Gets the ControlsMapping instance 
        /// </summary>
        /// <returns></returns>
        public static ControlsMapping Instance()
        {
            lock (Threadlock)
            {
                if (_instance != null)
                {
                    return _instance;
                }
                _instance = new ControlsMapping();
                return _instance;
            }
        }

        /// <summary>
        /// Maps the Unity Input manager control names to a ControlName. 
        /// This gives a central point to easily change it later and add more.
        /// </summary>
        public void MapControlls()
        {
            //TODO: This can be made smarter with a JoyStick enum and a dictionary of them with the mapping dictionary.
            //TODO: reset Taken booleans.
            Keyboard = new Dictionary<JoyStickControl, string>();
            _joystick1 = new Dictionary<JoyStickControl, string>();
            _joystick2 = new Dictionary<JoyStickControl, string>();
            _joystick3 = new Dictionary<JoyStickControl, string>();
            _joystick4 = new Dictionary<JoyStickControl, string>();

            Keyboard[JoyStickControl.LeftStick_X] = "Keyboard_X";
            Keyboard[JoyStickControl.LeftStick_Y] = "Keyboard_Y";
            if (Application.isEditor)
            {
                _joystick1[JoyStickControl.LeftStick_X] = "Horizontal";
                _joystick1[JoyStickControl.LeftStick_Y] = "Vertical";
                _joystick1[JoyStickControl.RightStick_X] = "Rotate";

            }
            else
            {
                _joystick1[JoyStickControl.LeftStick_X] = "JoyStick1_LeftStick_X";
                _joystick1[JoyStickControl.LeftStick_Y] = "JoyStick1_LeftStick_Y";
                _joystick1[JoyStickControl.RightStick_X] = "JoyStick1_RightStick_X";
            }
            _joystick1[JoyStickControl.Button_A] = "JoyStick1_Button_A";
            _joystick1[JoyStickControl.Button_B] = "JoyStick1_Button_B";
            _joystick1[JoyStickControl.Button_X] = "JoyStick1_Button_Y";

            _joystick2[JoyStickControl.LeftStick_X] = "JoyStick2_LeftStick_X";
            _joystick2[JoyStickControl.LeftStick_Y] = "JoyStick2_LeftStick_Y";
            _joystick2[JoyStickControl.RightStick_X] = "JoyStick2_RightStick_X";
            _joystick2[JoyStickControl.Button_A] = "JoyStick2_Button_A";
            _joystick2[JoyStickControl.Button_B] = "JoyStick2_Button_B";
            _joystick2[JoyStickControl.Button_X] = "JoyStick2_Button_Y";

            _joystick3[JoyStickControl.LeftStick_X] = "JoyStick3_LeftStick_X";
            _joystick3[JoyStickControl.LeftStick_Y] = "JoyStick3_LeftStick_Y";
            _joystick3[JoyStickControl.RightStick_X] = "JoyStick3_RightStick_X";
            _joystick3[JoyStickControl.Button_A] = "JoyStick3_Button_A";
            _joystick3[JoyStickControl.Button_B] = "JoyStick3_Button_B";
            _joystick3[JoyStickControl.Button_X] = "JoyStick3_Button_Y";

            _joystick4[JoyStickControl.LeftStick_X] = "JoyStick4_LeftStick_X";
            _joystick4[JoyStickControl.LeftStick_Y] = "JoyStick4_LeftStick_Y";
            _joystick4[JoyStickControl.RightStick_X] = "JoyStick4_RightStick_X";
            _joystick4[JoyStickControl.Button_A] = "JoyStick4_Button_A";
            _joystick4[JoyStickControl.Button_B] = "JoyStick4_Button_B";
            _joystick4[JoyStickControl.Button_X] = "JoyStick4_Button_Y";
        }

        /// <summary>
        /// Get the Unity Input manager control name for a given ControlName to be used with UnityEngine.Input library.
        /// </summary>
        /// <returns></returns>
        public IDictionary<JoyStickControl, string> GetJoyStickMapping(JoyStick joyStick)
        {
            if (joyStick == JoyStick.JoyStick1)
            {
                _joyStick1IsTaken = true;
                return _joystick1;
            }
            else if (joyStick == JoyStick.JoyStick2)
            {
                _joyStick2IsTaken = true;
                return _joystick2;
            }
            else if (joyStick == JoyStick.JoyStick3)
            {
                _joyStick3IsTaken = true;
                return _joystick3;
            }
            else if (joyStick == JoyStick.JoyStick4)
            {
                _joyStick4IsTaken = true;
                return _joystick4;
            }
            return null;
        }

    }
}