﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public enum Display
    {
        Forward,
        Backwards,
        Right,
        Left,
        Main
    }

    public class CameraController
    {
        private IDictionary<Display, int> _displays;

        CameraController()
        {
            _displays = new Dictionary<Display, int>();
        }

        /// <summary>
        /// Setups the monitor relations and activates the monitors that will be used.
        /// </summary>
        public void SetupMonitors()
        {
            if (Application.isEditor)
            {
                //Editor monitor setup
                _displays[Display.Forward] = 1;
                _displays[Display.Backwards] = 3;
                _displays[Display.Right] = 2;
                _displays[Display.Left] = 4;
                _displays[Display.Main] = 0;
            }
            else
            {
                //TODO These mappings should be loaded from config. And hardcodes into editor.
                _displays[Display.Forward] = 1;
                _displays[Display.Backwards] = 3;
                _displays[Display.Right] = 2;
                _displays[Display.Left] = 4;
                _displays[Display.Main] = 0;
            }
            //Activates the monitors listed iin the _displays field.
            UnityEngine.Display.displays[_displays[Display.Forward]].Activate();
            UnityEngine.Display.displays[_displays[Display.Backwards]].Activate();
            UnityEngine.Display.displays[_displays[Display.Right]].Activate();
            UnityEngine.Display.displays[_displays[Display.Left]].Activate();
            UnityEngine.Display.displays[_displays[Display.Main]].Activate();
        }

        public void MapCameraToDisplay(Camera camera, Display display)
        {
            camera.targetDisplay = _displays[display];
        }
    }
}