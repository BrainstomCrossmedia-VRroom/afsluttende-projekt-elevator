﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.SceneManagement;
using Scene = UnityEngine.SceneManagement.Scene;

public class SceneController : MonoBehaviour {

    public IScene GetSceneScriptInstance()
    {
        //string sceneName = SceneManager.GetActiveScene().name;
        //Scene returnValue = Types.GetType("Scene" + sceneName);
        
        return GetComponent<IScene>();
    }
}
