﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.SceneAssets.JacobTestScene.Scripts;
using UnityEngine;

namespace Assets.Scripts
{
    class PlayerPointer : MonoBehaviour, IPlayerPointer
    {
        public float PointerHorizontalAcceleration;
        public float PointerVerticalAcceleration;

        private GameObject objectToPlayWith;
        private RectTransform _rectTransform;
        private Canvas _canvas;
        private List<Camera> CamerasInOrder; //The first camera is expected to be the forward one (Primary).

        private string _controllerX;
        private string _controllerY;
        private Camera _referenceCamera; //The camera that will be used as reference when using related camera methods.
        private bool _isCamerasSet;



        void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvas = GetComponentInParent<Canvas>();
            SetCameras();

            //var sceneManager = FindObjectOfType<JacobTestScene>().GetComponent<JacobTestScene>();
            //_controllerX = sceneManager.controller1[JoyStickControl.LeftStick_X];
            //_controllerY = sceneManager.controller1[JoyStickControl.LeftStick_Y];
            //objectToPlayWith = GameObject.Find("Sphere");
        }

        void Update()
        {
            if (_controllerX == null || _controllerY == null || !_isCamerasSet) return;
            PointerMovement();
            //Testing();
        }

        private void PointerMovement()
        {
            float vertical = Input.GetAxis(_controllerY);
            float horizontal = Input.GetAxis(_controllerX);
            //TODO: Add acceleration.
            _rectTransform.position = new Vector3(_rectTransform.position.x + horizontal * PointerHorizontalAcceleration, _rectTransform.position.y + vertical * PointerVerticalAcceleration, _rectTransform.position.z);

            //Controls the pointers movement at display edges
            float posX = transform.localPosition.x + _canvas.pixelRect.width / 2f;
            float posY = transform.localPosition.y + _canvas.pixelRect.height / 2f;
            if (posX >= _referenceCamera.pixelWidth * 4f)
            {
                _rectTransform.position -= new Vector3(_canvas.pixelRect.width, 0, 0);
            }
            else if (posX < 0f)
            {
                _rectTransform.position += new Vector3(_canvas.pixelRect.width, 0, 0);
            }
            if (posY > _referenceCamera.pixelHeight)
            {
                _rectTransform.position = new Vector3(_rectTransform.position.x, _referenceCamera.pixelHeight, _rectTransform.position.z);
            }
            else if (posY < 0f)
            {
                _rectTransform.position = new Vector3(_rectTransform.position.x, 0f, _rectTransform.position.z);
            }
        }

        private void SetCameras()
        {
            List<Camera> cameras = new List<Camera> { Capacity = 4 };
            for (int i = 0; i < 4; i++)
            {
                cameras.Add(new Camera());
            }
            try
            {
                GameObject mainCamera = GameObject.Find("Main Cameras");
                var camerasList = mainCamera.GetComponentsInChildren<Camera>();
                cameras[0] = camerasList.FirstOrDefault(n => n.name == "Camera Forward");
                cameras[1] = camerasList.FirstOrDefault(n => n.name == "Camera Right");
                cameras[2] = camerasList.FirstOrDefault(n => n.name == "Camera Backwards");
                cameras[3] = camerasList.FirstOrDefault(n => n.name == "Camera Left");
                _referenceCamera = cameras[0];
            }
            catch (Exception e)
            {
                print("Mapping cameras in PlayPointer failed. Error: " + e + "\n StackTrace: " + e.StackTrace);
                return;
            }
            CamerasInOrder = cameras;
            _isCamerasSet = true;
        }

        private int GetActiveCamera()
        {
            Camera camera = CamerasInOrder[0]; //The four cameras
            Vector3 position = transform.localPosition; //Pointer localPosition on canvas
            //Canvas coord 0,0 is the center of the screen, while camera coord 0,0 is bottom left corner.
            float posX = position.x + _canvas.pixelRect.width / 2f;
            int camInt = 0;

            if (posX >= 0f && posX < camera.pixelWidth * 1f)
            {
                camInt = 0;
            }
            else if (posX >= camera.pixelWidth && posX < camera.pixelWidth * 2f)
            {
                camInt = 1;
            }
            else if (posX >= camera.pixelWidth * 2f && posX < camera.pixelWidth * 3f)
            {
                camInt = 2;
            }
            else if (posX >= camera.pixelWidth * 3f && posX < camera.pixelWidth * 4f)
            {
                camInt = 3;
            }
            return camInt;
        }

        private Ray CreateRay()
        {
            int cameraInt = GetActiveCamera();
            Camera camera = CamerasInOrder[cameraInt];
            Vector3 position = transform.localPosition; //Pointer localPosition on canvas
            float posX = position.x + _canvas.pixelRect.width / 2f - camera.pixelWidth * cameraInt;
            float posY = position.y + _canvas.pixelRect.height / 2f;
            Vector3 rayPostion = new Vector3(posX / camera.pixelWidth, posY / camera.pixelHeight, z: 0);
            Ray ray = camera.ViewportPointToRay(rayPostion);
            //Vector3 rayPostion = new Vector3(posX, posY, z: 0);
            //Ray ray = camera.ScreenPointToRay(rayPostion);
            return ray;
        }

        private void Testing()
        {
            Ray ray = CreateRay();

            Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            if (IsInteractiveObjectInFront())
            {
                print("YES! The object is interactive.");
            }
            else
            {
                print("NO! The object isn't interactive.");
            }
            //Vector3 between = ray.origin - ray.direction;
            //Vector3 ninetyDegrees = RotateAroundPivot(ray.GetPoint(5),ray.origin, Quaternion.Euler(0,90,0));
            //ray = new Ray(ray.origin, ninetyDegrees);
            Vector3 newSpot = ray.GetPoint(5);
            objectToPlayWith.transform.position = newSpot;

            //print("Camera: " + camera.name.Remove(0, 7) + " - cameraPosition: " + rayPostion + " - pointerPosition: " + transform.localPosition + " - Ray: " + ray.direction + " - Ball: " + newSpot);

            //Camera camera = GameObject.Find("Camera Right").GetComponent<Camera>();
            //var camerapos = Camera.main.ScreenToWorldPoint(_rectTransform.position);
            //RaycastHit hit;
            //Ray ray = new Ray(camerapos, transform.TransformDirection(Vector3.forward));
            //Debug.DrawRay(camerapos, transform.TransformDirection(Vector3.forward), Color.red);
            //if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            //{
            //    print("I Hit: " + hit.transform.name);
            //    IInteractive interactive = hit.collider.gameObject.GetComponent<IInteractive>();
            //    if (interactive == null) return; //Exits method as the object doesn't implement the required interface IInteractive
            //    interactive.OnUserOver();
            //    interactive.OnUserSelect();
            //}
        }

        public void SetControlls(string controllerXAxis, string controllerYAxis)
        {
            _controllerX = controllerXAxis;
            _controllerY = controllerYAxis;
        }

        public bool IsInteractiveObjectInFront()
        {
            IInteractive interactiveObject = GetInteractiveObjectInFront();
            if (interactiveObject != null) return true;
            return false;
        }

        public IInteractive GetInteractiveObjectInFront()
        {
            if (!_isCamerasSet) return null;
            Ray ray = CreateRay();

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                //print("I Hit: " + hit.transform.name);
                IInteractive interactive = hit.collider.gameObject.GetComponent<IInteractive>();
                if (interactive != null) return interactive; //Returns the object if it implements the required interface IInteractive
            }
            return null;
        }
    }
}
