﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    internal class Monitor
    {
        public Display GameDisplay { get; set; }
        public int SystemDisplayIndex { get; set; }

    }
    internal class Config
    {
        public IList<Monitor> Monitors { get; set; }
    }
}
