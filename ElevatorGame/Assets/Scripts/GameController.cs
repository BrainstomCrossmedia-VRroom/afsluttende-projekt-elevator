﻿using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class GameController : MonoBehaviour
    {
        enum Scenes
        {
            None,
            JacobTestScene,
            MemoryFlippingGame,
            ExampleScene1
        }

        private const float SceneChangeWaitTime = 5;

        public ControlsMapping ControlsMapping { get; private set; }
        public bool IsSceneReady { get; private set; }

        private static GameController _instance;
        private bool _waitTimePassed;
        private IScene _activeSceneController;
        private Scenes _activeSceneEnum;
        private AsyncOperation _preparedScene;

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                DestroyImmediate(gameObject);
                return;
            }
            _instance = this;
            DontDestroyOnLoad(gameObject);
            //if (_instance == null)
            //{
            //    DontDestroyOnLoad(gameObject);
            //    _instance = this;
            //}
            //else if (_instance != this)
            //{
            //    Destroy(gameObject);
            //}

            //Other awake code here:
            SetupMonitors();
            ControlsMapping = ControlsMapping.Instance();
            //new ConfigController().Go();
            if (Application.isEditor)
            {
                //_activeScene = UnityEngine.SceneManagement.Scene
            }
            _activeSceneEnum = Scenes.MemoryFlippingGame;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Setups the monitors for the scene(s) to map there cameras to later. This happens once at startup.
        /// </summary>
        private static void SetupMonitors()
        {

        }

        public IScene GetSceneScriptInstance()
        {
            //string sceneName = SceneManager.GetActiveScene().name;
            //Scene returnValue = Types.GetType("Scene" + sceneName);

            var returnValue = gameObject.GetComponent<IScene>();
            return returnValue;
        }

        #region SceneManagement

        public void ChangeScene()
        {
            //The curated Scene list is currently just written into this method. 
            //TODO: Make ChangeScene switch scenes from a curated list of Scenes.
            Scenes nextScene = Scenes.None;
            if (_activeSceneEnum == Scenes.MemoryFlippingGame)
            {
                nextScene = Scenes.ExampleScene1;
            }
            if (_activeSceneEnum == Scenes.ExampleScene1)
            {
                nextScene = Scenes.JacobTestScene;
            }
            LoadScene(nextScene, true);
            _activeSceneEnum = nextScene;
        }

        //public void GameOver()
        //{
        //    SceneManager.LoadScene("GameOver");
        //}

        //public void GameWon()
        //{
        //    SceneManager.LoadScene("GameWon");
        //}

        private void LoadScene(Scenes scene, bool switchWhenReady)
        {
            LoadScene(scene, LoadSceneMode.Single, switchWhenReady);
        }
        private void LoadScene(Scenes scene, LoadSceneMode loadSceneMode, bool switchWhenReady)
        {
            if (scene == Scenes.None)
            {
                Debug.Log("Scene load refused. Trying to load a scene called None, will do horrible stuff.");
                return;
            }
            Debug.Log("Killing any other instances of scene loading that may be active."); //Killing them just in case. As they'll brake stuff horribly if they exist.
            StopCoroutine("LoadSceneAsync");
            StopCoroutine("WaitSeconds");
            StopCoroutine("SwithToSceneWhenReady");

            _preparedScene = null;
            StartCoroutine(LoadSceneAsync(scene, loadSceneMode)); //Starting load of scene.
            if (switchWhenReady) //If it should load automaticly when ready.
            {
                _waitTimePassed = false;
                StartCoroutine(WaitSeconds()); //Starts the timer, to make sure all scene loads takes a minimum amount of time.
                StartCoroutine(SwithToSceneWhenReady());
            }
        }

        /// <summary>
        /// Switches to the scene whenever it's ready but after a minimum amount of time.
        /// </summary>
        /// <returns></returns>
        private IEnumerator SwithToSceneWhenReady()
        {
            while (!IsSceneReady || !_waitTimePassed)
            {
                yield return null;
            }
            SwitchToloadedScene();
        }

        /// <summary>
        /// Waits a defined amount of time. Used to make sure all scenes takes a minimum amount of time to "load".
        /// </summary>
        /// <returns>The Enumerator</returns>
        private IEnumerator WaitSeconds()
        {
            yield return new WaitForSeconds(SceneChangeWaitTime);
            _waitTimePassed = true;
        }

        /// <summary>
        /// The Courotine that handles the loading of the scene, without stalling the program.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="loadSceneMode"></param>
        /// <returns></returns>
        private IEnumerator LoadSceneAsync(Scenes scene, LoadSceneMode loadSceneMode)
        {
            Debug.Log("Starting to load scene: " + scene);
            IsSceneReady = false;
            _preparedScene = SceneManager.LoadSceneAsync(scene.ToString(), loadSceneMode);
            _preparedScene.allowSceneActivation = false;

            //While loop is necessary as the scene Done method doesn't work the way it should. 
            //Makes sure the scene is 90 % loaded, and then continues.
            while (_preparedScene.progress < 0.9f)
            {
                Debug.Log("Loading scene.. " + _preparedScene.progress * 100 + "%");
                yield return null;
            }
            Debug.Log("Loading scene.. 100%");
            IsSceneReady = true;

            while (!_preparedScene.isDone) //Will only become true when the scene load is 90% and allowed to activate.
            {
                yield return _preparedScene; //Finalizes the scene activation.
            }
        }
        public bool SwitchToloadedScene()
        {
            if (IsSceneReady)
            {
                Debug.Log("Switching to loaded scene.");
                _preparedScene.allowSceneActivation = true; //If this is called before the scene is 90% loaded, it won't work.
                return true;
            }
            Debug.Log("The Scene isn't ready yet.");
            return false;
        }
        #endregion
    }
}
