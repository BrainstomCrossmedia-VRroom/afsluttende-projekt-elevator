﻿namespace Assets.Scripts
{
    public interface IInteractive
    {
        //void OnUserSelect(); //TODO: Remove
        //void OnUserOver(); //TODO: Remove
        //void OnUserSelect(JoyStick joyStick, JoyStickControl joyStickControls);
        //void OnUserOver(JoyStick joyStick);

        void Interact();
    }
}
