﻿using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

namespace Assets.SceneAssets.MemoryFlippingGame.Scripts
{
    public class SceneManager : MonoBehaviour
    {
        private IPlayerPointer _playerPointer;
        private GameController _gameController;
        private IDictionary<JoyStickControl, string> _controls;
        private PhotoCubeScript _photoCubeScript;
        // Use this for initialization
        void Start()
        {
            _playerPointer = GameObject.FindObjectOfType<PlayerPointer>();
            _gameController = GameObject.FindObjectOfType<GameController>();
            _photoCubeScript = GameObject.FindObjectOfType<PhotoCubeScript>();
            _controls = _gameController.ControlsMapping.GetJoyStickMapping(JoyStick.JoyStick1);
            _playerPointer.SetControlls(_controls[JoyStickControl.LeftStick_X], _controls[JoyStickControl.LeftStick_Y]);
        }

        public void WinGame()
        {
            GameObject.Find("Elevator").GetComponent<ElevatorController>().CloseDoors = true;
            _gameController.ChangeScene();
        }

        void Update () {
            if (Input.GetButtonDown(_controls[JoyStickControl.Button_A]))
            {
                IInteractive currentObjectInFront = _playerPointer.GetInteractiveObjectInFront();
                if (currentObjectInFront != null && _photoCubeScript.MayInteract())
                    currentObjectInFront.Interact();
            }
        }
        
    }
}
