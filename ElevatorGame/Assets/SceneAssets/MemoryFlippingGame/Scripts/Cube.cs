﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace Assets.SceneAssets.MemoryFlippingGame.Scripts
{
    public class Cube : MonoBehaviour, IInteractive
    {
        private Sprite image;
        private Animator anim;
        private bool hidden = true;
        private PhotoCubeScript _photoCubeScript;
        // Use this for initialization
        void Start()
        {
            _photoCubeScript = GetComponentInParent<PhotoCubeScript>();
            anim = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
        }

        public Sprite GetImage()
        {
            return image;
        }

        public void SetImage(Sprite newImage)
        {
            image = newImage;
            gameObject.GetComponent<SpriteRenderer>().sprite = newImage;
        }

        public bool IsHidden()
        {
            return hidden;
        }

        public void TurnCube()
        {
            if (hidden)
            {
                anim.SetBool("Hidden", false);
                hidden = false;
            }
            else if (!hidden)
            {
                anim.SetBool("Hidden", true);
                hidden = true;
            }
        }

        public void Interact()
        {
            _photoCubeScript.TurnSpecificCube(this.transform);
        }
    }
}