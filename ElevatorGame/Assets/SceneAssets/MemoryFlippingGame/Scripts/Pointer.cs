﻿using UnityEngine;
using System.Collections;
using Assets.SceneAssets.MemoryFlippingGame.Scripts;

public class Pointer : MonoBehaviour {
    private PhotoCubeScript _photoCubeScript;

	// Use this for initialization
	void Start ()
	{
	    _photoCubeScript = GameObject.Find("Cubes").GetComponent<PhotoCubeScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        Transform other = collider.gameObject.transform;
        _photoCubeScript.TurnSpecificCube(other);
    }
}
