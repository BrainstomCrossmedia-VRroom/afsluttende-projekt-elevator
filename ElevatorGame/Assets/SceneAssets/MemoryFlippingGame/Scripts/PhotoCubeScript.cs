﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.SceneAssets.MemoryFlippingGame.Scripts
{
    public class PhotoCubeScript : MonoBehaviour
    {
        private List<Sprite> _imageList;
        private IList<Transform> _allCubes;
        private Cube _cubeScript;
        private int _cubesTurned = 0;
        private bool _mayInteract = true;
        private IList<Transform> _turnedCubes;

        public Boolean turn = false;



        // Use this for initialization
        void Awake()
        {
            _cubeScript = gameObject.GetComponentInChildren<Cube>();
            _allCubes = GameObject.Find("Cubes").GetComponentsInChildren<Transform>().Where(n => n.name.Contains("PhotoCube")).ToList();
            _imageList = new List<Sprite>(gameObject.GetComponent<PhotoScript>().GetRandomImageList());
            _turnedCubes = new List<Transform>();
            int k = _imageList.Count;
            foreach (var i in _allCubes)
            {
                int j = Random.Range(0, k);
                Sprite image = _imageList[j];
                i.gameObject.GetComponent<Cube>().SetImage(image);
                _imageList.RemoveAt(j);
                k--;
            }
        }

        void Start()
        {
            StartCoroutine(DidWeWin());
        }

        // Update is called once per frame
        void Update()
        {
            if (turn)
            {
                TurnAllCubes();
            }
            turn = false;

            if (_cubesTurned == 3)
            {
                _mayInteract = false;
                StartCoroutine(CubeShowWait());
                _cubesTurned = 0;
                _mayInteract = true;
            }
            //if (_allCubes.Count == 54) //TODO: Fix number from 57 to 0!
            //{
            //    //TODO: implement scene change!
            //    Debug.Log("YOU WIN!");
            //    GameObject.Find("SceneManager").GetComponent<SceneManager>().WinGame();
            //}
        }

        public bool MayInteract()
        {
            return _mayInteract;
        }

        IEnumerator DidWeWin()
        {
            while (_allCubes.Count > 54) //TODO: Fix number from 57 to 0!
            {
                yield return null;
            }
            Debug.Log("YOU WIN!");
            GameObject.Find("SceneManager").GetComponent<SceneManager>().WinGame();
        }

        IEnumerator CubeShowWait()
        {
            yield return new WaitForSeconds(2);
            if (!CheckImageOnTurnedCubes())
            {
                TurnWrongCubes();
            }
            else
            {
                DestroyCorrectCubes();
            }
        }

        public void TurnSpecificCube(Transform cube)
        {
            int size = _allCubes.Count;
            bool found = false;
            for (int i = 0; i < size; i++)
            {
                if (cube == _allCubes[i].transform)
                {
                    Cube currentCube = _allCubes[i].gameObject.GetComponent<Cube>();
                    if (!currentCube.IsHidden())
                    {
                        return;
                    }
                    currentCube.TurnCube();
                    _turnedCubes.Add(_allCubes[i]);
                    _cubesTurned++;
                }
            }
        }

        private void TurnAllCubes()
        {
            foreach (var i in _allCubes)
            {
                i.gameObject.GetComponent<Cube>().TurnCube();

            }
            //    else
            //    {
            //        foreach (var i in _allCubes)
            //        {
            //            i.gameObject.GetComponent<Cube>().TurnCube(false);
            //        }
            //    }
        }

        private void DestroyCorrectCubes()
        {
            for (int i = 0; i < _turnedCubes.Count; i++)
            {
                Debug.Log(_allCubes.Count);
                _allCubes.Remove(_turnedCubes[i]);
                Debug.Log(_allCubes.Count);
                Destroy(_turnedCubes[i].gameObject);
                Debug.Log(_allCubes.Count);
                Debug.Log("---");
            }
            _turnedCubes.Clear();
        }

        private bool CheckImageOnTurnedCubes()
        {
            bool returnBool = false;
            Sprite image = _turnedCubes[0].gameObject.GetComponent<Cube>().GetImage();
            if (image == _turnedCubes[1].gameObject.GetComponent<Cube>().GetImage() &&
                image == _turnedCubes[2].gameObject.GetComponent<Cube>().GetImage())
            {
                returnBool = true;
            }
            return returnBool;
        }

        private void TurnWrongCubes()
        {
            for (int i = 0; i < _turnedCubes.Count; i++)
            {
                _turnedCubes[i].gameObject.GetComponent<Cube>().TurnCube();
            }
            _turnedCubes.Clear();
        }
    }
}
