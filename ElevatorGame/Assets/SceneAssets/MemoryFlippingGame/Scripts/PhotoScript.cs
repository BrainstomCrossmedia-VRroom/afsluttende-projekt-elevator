﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneAssets.MemoryFlippingGame.Scripts
{
    public class PhotoScript : MonoBehaviour
    {
        public List<Sprite> images;
        private List<Sprite> _randomImageList;

        public List<Sprite> GetImageList()
        {
            return images;
        }

        public List<Sprite> GetRandomImageList()
        {
            _randomImageList = new List<Sprite>();
            Debug.Log(images.Count);
            foreach (var i in images)
            {
                _randomImageList.Add(i);
                _randomImageList.Add(i);
                _randomImageList.Add(i);
            }
            
            return _randomImageList;
        }
    }
}
