﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.SceneAssets.MemoryFlippingGame.Scripts;
using Assets.Scripts;

public class ExampleSceneController : MonoBehaviour
{
    private IPlayerPointer _playerPointer;
    private IDictionary<JoyStickControl, string> _controls;
    private GameController _gameController;
    // Use this for initialization
    void Start()
    {
        _playerPointer = GameObject.FindObjectOfType<PlayerPointer>();
        _gameController = GameObject.FindObjectOfType<GameController>();
        _controls = _gameController.ControlsMapping.GetJoyStickMapping(JoyStick.JoyStick1);
        _playerPointer.SetControlls(_controls[JoyStickControl.LeftStick_X], _controls[JoyStickControl.LeftStick_Y]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(_controls[JoyStickControl.Button_A]))
        {
            IInteractive currentObjectInFront = _playerPointer.GetInteractiveObjectInFront();
            currentObjectInFront.Interact();  
        }
    }

    public void WinGame()
    {
        GameObject.Find("Elevator").GetComponent<ElevatorController>().CloseDoors = true;
        _gameController.ChangeScene();
    }
}
