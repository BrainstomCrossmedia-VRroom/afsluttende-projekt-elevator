﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class ExampleTrigger : MonoBehaviour, IInteractive
{


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Interact()
    {
        GameObject.Find("SceneController").GetComponent<ExampleSceneController>().WinGame();
    }
}
