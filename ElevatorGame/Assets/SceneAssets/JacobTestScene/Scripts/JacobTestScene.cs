﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

namespace Assets.SceneAssets.JacobTestScene.Scripts
{
    public class JacobTestScene : MonoBehaviour, IScene
    {


        private GameController _gameMaster;
        public IDictionary<JoyStickControl, string> controller1 { get; private set; }
        // Use this for initialization
        void Awake()
        {
            _gameMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            print(_gameMaster.ToString());
            controller1 = _gameMaster.ControlsMapping.GetJoyStickMapping(JoyStick.JoyStick1);
            Debug.Log("Loaded: " + gameObject.name);
        }

        // Update is called once per frame
        void Update()
        {

        }

        void FixedUpdate()
        {

        }

        
    }
}
